# app-vue-cube

### 基本命令

- 项目初始化

```
npm install
```

- 本地开发编译

```
npm run serve
```

- 打包生产环境

```
npm run build
```

- 打包测试环境（准生产）

```
npm run build-test
```

- 生成编译结果分析

```
npm run report
```

- Run your tests

```
npm run test
```

- Lints and fixes files

```
npm run lint
```

- Run your unit tests

```
npm run test:unit
```

---

### 参考资料

- [vue-cli](https://cli.vuejs.org/config/)
- [vue](https://cn.vuejs.org/)
- [vue-router](https://router.vuejs.org/zh/)
- [vue 开发风格指南](https://cn.vuejs.org/v2/style-guide/)
- [cube-ui](https://didi.github.io/cube-ui/#/zh-CN/docs/introduction)
- [moment.js 日期格式化](http://momentjs.cn/docs/#/use-it/)
- [lodash 对象操作](https://www.lodashjs.com/)
- [axios 异步请求库](https://www.kancloud.cn/yunye/axios/234845)
- query-string

  ```
  import qs from "query-string";
  //获取所有的查询参数，获取到的是一个对象，key-value都是string
  let queryParams = qs.parse(location.search);
  //需要将其中的JSONString转换为对象
  let _data = JSON.parse(queryParams._data);
  ```
