module.exports = {
  plugins: {
    autoprefixer: {},
    // 使用 px转rem
    'postcss-px2rem': {
      remUnit: 37.5
    }
  }
};
