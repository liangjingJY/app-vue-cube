/* 
 * 判断打包时接口请求地址
 * @Author: 'LiangJ' 
 * @Date: 2018-12-19 13:08:10 
 * @Last Modified by: LiangJ
 * @Last Modified time: 2019-01-04 19:10:34
 */
let baseUrl = '';

const env =
  process.env.NODE_ENV === 'development'
    ? 'development'
    : process.env.VUE_APP_TITLE === 'test'
      ? 'test'
      : 'production';
switch (env) {
  case 'development':
    baseUrl = '';
    break;
  case 'test':
    baseUrl = 'https://221.192.1.84:9095/SSPMobileServer';
    break;
  case 'production':
    baseUrl = 'https://221.192.1.84:8486/SSPMobileServer';
    break;
}

export default baseUrl;
