import Vue from 'vue';

Vue.config.errorHandler = function(err, vm, info) {
  if (vm) {
    var componentName = formatComponentName(vm);
    var propsData = vm.$options && vm.$options.propsData;
    console.log(`componentName=====${componentName}`);
    console.log(`propsData=====${propsData}`);
    console.log(`info=====${info}`);
    console.error(`err=====${err}`);
  } else {
    console.log(err);
    console.log(`info=====${info}`);
  }
};

function formatComponentName(vm) {
  if (vm.$root === vm) return 'root';

  var name = vm._isVue
    ? (vm.$options && vm.$options.name) ||
      (vm.$options && vm.$options._componentTag)
    : vm.name;
  return (
    (name ? 'component <' + name + '>' : 'anonymous component') +
    (vm._isVue && vm.$options && vm.$options.__file
      ? ' at ' + (vm.$options && vm.$options.__file)
      : '')
  );
}
