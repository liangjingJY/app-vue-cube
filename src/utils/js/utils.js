/**
 * 判断是否为空字符串或空对象
 * @param {Object} param
 */
import _isEmpty from 'lodash/isEmpty';
let isEmpty = function(param) {
  if (param === undefined) {
    return true;
  }
  if (param === null) {
    return true;
  }
  if (param === '') {
    return true;
  }
  if (_isEmpty(param)) {
    return true;
  }
  return false;
};

export { isEmpty };
