//用一个iframe向原生代码发送urlScheme
function callObject(uri) {
  console.log(uri);
  var BAEIframe = document.createElement('iframe');
  BAEIframe.style.display = 'none';
  BAEIframe.src = uri;
  document.documentElement.appendChild(BAEIframe);
  setTimeout(function() {
    document.documentElement.removeChild(BAEIframe);
  }, 0);
}
var clickFlag = true;
let boncAppEngine = {
  //设备对象
  device: {
    //原生获取设备信息成功的回调句柄，其中包括用户的经度、纬度、移动速度及其他
    successHandler: function() {},
    //原生获取设备信息失败的回调句柄
    errorHandler: function() {},
    //js调用获取设备信息
    getDeviceInfo: function(success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=device&command=getDeviceInfo';
      callObject(uri);
    },
    //原生设备震动完成之后的回调
    vibrateFinishHandler: function() {},
    //js调用设备震动
    vibrate: function(repeatCount, finish) {
      var uri =
        'mobile-service://?object=device&command=vibrate&params=[' +
        repeatCount +
        ']';
      this.vibrateFinishHandler = finish;
      callObject(uri);
    }
  },
  //定位对象locationManager；
  locationManager: {
    //原生定位成功的回调句柄，其中包括用户的经度、纬度、移动速度及其他
    successHandler: function() {},
    //原生定位失败的回调句柄
    errorHandler: function() {},
    //js调用开始定位，原生平台（ios、android）会通过原生代码调用handler回调
    start: function(success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=locationManager&command=start';
      callObject(uri);
    },
    //js调用停止定位（当页面不在需要定位功能时，请务必调用此函数关闭定位，保持电量）
    stop: function() {
      var uri = 'mobile-service://?object=locationManager&command=stop';
      callObject(uri);
    }
  },
  //加速度计
  accelerometer: {
    //原生回调返回加速度计信息的句柄
    successHandler: function() {},
    //原生回调错误信息的句柄
    errorHandler: function() {},
    //js调用开始采集加速度信息
    start: function(success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=accelerometer&command=start';
      callObject(uri);
    },
    //js调用停止采集加速度信息
    stop: function() {
      var uri = 'mobile-service://?object=accelerometer&command=stop';
      callObject(uri);
    }
  },
  //相机需要获取的媒体类型
  mediaType: {
    JPEG: 0,
    PNG: 1
  },
  //相机对象camera
  camera: {
    //原生拍照成功的回调句柄，其中包括照片数据Base64编码的字符串，以及照片的类型；
    successHandler: function() {},
    //原生拍照发生错误的回调句柄
    errorHandler: function() {},
    //原生用户取消拍照的回调句柄
    cancelHandler: function() {},
    //js调用拍照
    takePhoto: function(mediaType, quality, success, cancle, error) {
      this.successHandler = success;
      this.cancleHandler = cancle;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=camera&command=takePhoto&params=[' +
        mediaType +
        ',' +
        quality +
        ']';
      callObject(uri);
    }
  },
  //手机通讯录对象contacts
  //    contacts: {
  //        //原生创建联系人成功的回调句柄
  //        newContactSuccessHandler: function(conatactInfo) {},
  //        //原生创建联系人取消的回调句柄
  //        newContactCancleHandler: function() {},
  //        //原生访问联系人出错的回到句柄
  //        newContactErrorHandler: function(errror) {},
  //        //js调用获取联系人
  //        newContact: function(success, cancle, error) {
  //            this.newContactSuccessHandler = success;
  //            this.newContactCancleHandler = cancle;
  //            this.newContactErrorHandler = error;
  //            var uri = 'mobile-service://?object=contacts&command=newContact';
  //            callObject(uri);
  //        },
  //        //选择联系人成功的回调句柄
  //        chooseContactSuccessHandler: function(conatactInfo) {},
  //        //选择联系人取消的回调句柄
  //        chooseContactCancleHandler: function() {},
  //        //选择联系人错误的回调句柄
  //        chooseContactErrorHandler: function(errror) {},
  //        //js调用选择联系人
  //        chooseContact: function(success, cancle, error) {
  //            this.chooseContactSuccessHandler = success;
  //            this.chooseContactCancleHandler = cancle;
  //            this.chooseContactErrorHandler = error;
  //            var uri = 'mobile-service://?object=contacts&command=chooseContact';
  //            callObject(uri);
  //        }
  //    },
  //二维码扫描器对象codeScanner；
  codeScanner: {
    //原生扫描成功的回调句柄，其中包括用户的经度、纬度、移动速度及其他
    successHandler: function() {},
    //原生扫描失败的回调句柄
    errorHandler: function() {},
    //原生用户取消扫描操作的回调句柄
    cancleHandler: function() {},
    //js调用扫描
    scan: function(success, cancle, error) {
      this.successHandler = success;
      this.cancleHandler = cancle;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=codeScanner&command=scan';
      callObject(uri);
    }
  },
  //组织结构树成员选择
  //    organizerPicker: {
  //        //原生选择成员成功的回调句柄，其中包括用户的经度、纬度、移动速度及其他
  //        successHandler: function(memberList) {},
  //        //原生选择成员失败的回调句柄
  //        errorHandler: function(error) {},
  //        //原生选择成员取消扫描操作的回调句柄
  //        cancleHandler: function() {},
  //        //js调用扫描
  //        show: function(selections, multiSelectionEnabled, success, cancle, error) {
  //            this.successHandler = success;
  //            this.cancleHandler = cancle;
  //            this.errorHandler = error;
  //            var arrStr = '';
  //            if (selections instanceof Array) {
  //                selections.forEach(function(item, index) {
  //                    arrStr = arrStr + '"' + item + '"';
  //                    if (index !== selections.length - 1) {
  //                        arrStr = arrStr + ',';
  //                    }
  //                })
  //            }
  //            var uri = 'mobile-service://?object=organizerPicker&command=show&params=[[' + arrStr + '],' + multiSelectionEnabled + ']';
  //            callObject(uri);
  //        }
  //    },
  webNavigation: {
    successHandler: function() {},
    errorHandler: function() {},
    //点击右上角导航后的操作
    parentItemHandler: function() {},
    //点击展出的弹出框的操作
    childItemHandler: function() {},
    //navigationBg控制背景，navigationModule控制导航栏显示
    setNaviagtionShowStyle: function(navigationSetting, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=webNavigation&command=navigationShowStyle&params= ' +
        JSON.stringify(navigationSetting) +
        '';
      callObject(uri);
    }
  },
  //调用批注分享功能
  remarkAndShare: function() {
    var uri = 'mobile-service://?object=screen&command=remarkAndShare';
    callObject(uri);
  },

  //页面跳转
  /*redirectPage: {
		//网页跳转原生
		webEnterNative: function(enterInfo, success, error) {
			this.successHandler = success;
			this.errorHandler = error;
			var uri = 'mobile-service://?object=redirectPage&command=webEnterNative&params= ' + JSON.stringify(enterInfo) + '';
			callObject(uri);
		}
	},*/
  redirectPage: {
    // 调转三方应用
    jumpThirdApp: function(info) {
      var uri =
        'mobile-service://?object=redirectPage&command=jumpThirdApp&params=[' +
        info +
        ']';
      if (clickFlag) {
        clickFlag = false;
        callObject(uri);
      }
      //设置点击间隔时间
      setTimeout(function() {
        clickFlag = true;
      }, 2000);
    },
    //绑定成功
    bindSuccess: function(info) {
      var uri =
        'mobile-service://?object=redirectPage&command=bindSuccse&params=[' +
        info +
        ']';
      callObject(uri);
    },
    //跳转绑定页面
    jumpBind: function(info) {
      var uri =
        'mobile-service://?object=redirectPage&command=jumpBind&params=[' +
        info +
        ']';
      callObject(uri);
    },
    //数据加载失败 重新加载数据
    reloadData: function(info) {
      var uri =
        'mobile-service://?object=redirectPage&command=reloadData&params=[' +
        info +
        ']';
      callObject(uri);
    },
    //注册成功回调ATOK
    registerSuccess: function() {
      var uri = 'mobile-service://?object=redirectPage&command=registerSuccess';
      callObject(uri);
    },
    //切换角色
    changeRole: function() {
      var uri = 'mobile-service://?object=redirectPage&command=changeRole';
      callObject(uri);
    },
    //关闭当前页面
    closePresentWindow: function() {
      var uri =
        'mobile-service://?object=redirectPage&command=closePresentWindow';
      callObject(uri);
    }
  },
  serveProtocol: {
    // 同意保密服务协议
    agreeSuccess: function(info) {
      var uri =
        'mobile-service://?object=serveProtocol&command=agreeSuccess&params=[' +
        info +
        ']';
      callObject(uri);
    },
    disAgree: function(info) {
      var uri =
        'mobile-service://?object=serveProtocol&command=disAgree&params=[' +
        info +
        ']';
      callObject(uri);
    }
  },
  getSomekeys: {
    // 获取秘钥
    getSecretkeyHandler: function() {},
    errorHandler: function() {},
    getSecretkey: function(success, error) {
      this.getSecretkeyHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=getSomekeys&command=getSecretkey';
      callObject(uri);
    },
    getATOKHandler: function() {},
    getATOK: function(success, error) {
      this.getATOKHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=getSomekeys&command=getATOK';
      callObject(uri);
    }
  },
  permission: {
    //获取第三方应用权限，如果未获取将无法调用其他js方法
    successHandler: function() {},
    errorHandler: function() {},
    getPermissionInfo: function(data, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=permission&command=getPermission&params= [' +
        data +
        ']';
      callObject(uri);
    }
  },
  soundRecording: {
    //原生录音成功的回调句柄，
    successHandler: function() {},
    //原生录音失败的回调句柄
    errorHandler: function() {},
    //js调用开始录音，原生平台（ios、android）会通过原生代码调用handler回调
    start: function(info, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=soundRecording&command=start&params= [' +
        info +
        ']';
      callObject(uri);
    },
    //js调用停止录音
    stop: function() {
      var uri = 'mobile-service://?object=soundRecording&command=stop';
      callObject(uri);
    },
    //js调用取消录音
    cancel: function() {
      var uri = 'mobile-service://?object=soundRecording&command=cancel';
      callObject(uri);
    }
  },
  alarmClock: {
    //调用原生闹钟成功的回调句柄，
    successHandler: function() {},
    //调用原生闹钟失败的回调句柄
    errorHandler: function() {},
    //js调用开始录音，原生平台（ios、android）会通过原生代码调用handler回调
    start: function(info, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=alarmClock&command=start&params= [' +
        info +
        ']';
      callObject(uri);
    }
  },
  //视频全屏
  setFullScreen: {
    //调用原生闹钟成功的回调句柄，
    successHandler: function() {},
    //调用原生闹钟失败的回调句柄
    errorHandler: function() {},
    //js调用开始录音，原生平台（ios、android）会通过原生代码调用handler回调
    start: function(info, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=video&command=setFullScreen&params= [' +
        info +
        ']';
      callObject(uri);
    }
  },
  //视频退出全屏
  quitFullScreen: {
    //调用原生闹钟成功的回调句柄，
    successHandler: function() {},
    //调用原生闹钟失败的回调句柄
    errorHandler: function() {},
    //js调用开始录音，原生平台（ios、android）会通过原生代码调用handler回调
    start: function(info, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=video&command=quitFullScreen&params= [' +
        info +
        ']';
      callObject(uri);
    }
  },
  refreshPage: {
    cardCell: function(data) {
      //通过menuId刷新卡片data数据格式为 {menuIds:[192,293]}
      var uri =
        'mobile-service://?object=refreshPage&command=cardCell&params= [' +
        data +
        ']';
      callObject(uri);
    }
  },

  //文件
  file: {
    //查看文件 fileObj中文件地址对应的key为fileUrl
    successHandler: function() {},
    errorHandler: function() {},
    viewFile: function(fileObj, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=file&command=viewFile&params=' + fileObj + '';
      callObject(uri);
    }
  },

  //关闭
  shutdown: function() {
    var uri = 'mobile-service://?object=self&command=shutdown';
    callObject(uri);
  },
  // 切换用户的id
  changeUserId: {
    successHandler: function() {},
    errorHandler: function() {},
    changeUserIdStr: function(info, success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri =
        'mobile-service://?object=changeUserId&command=changeUserIdStr&params=[' +
        info +
        ']';
      callObject(uri);
    }
  },
  //退出登录
  loginOut: function() {
    var uri = 'mobile-service://?object=self&command=loginOut';
    callObject(uri);
  },
  //获取版本号
  appInfo: {
    //原生获取应信息成功的回调句柄，其中包括应用的版本
    successHandler: function() {},
    //原生获取应用信息失败的回调句柄
    errorHandler: function() {},
    //js调用获取应用的版本号
    getAppVersion: function(success, error) {
      this.successHandler = success;
      this.errorHandler = error;
      var uri = 'mobile-service://?object=appInfo&command=getAppVersion';
      callObject(uri);
    }
  }
  /*// 页面刷新
	refreshPage: {
		cardCell: function(data) { //通过menuId刷新卡片data数据格式为 {menuIds:['192','293']}
			alert(JSON.stringify(data));
			var uri = 'mobile-service://?object=refreshPage&command=cardCell&params= [' + data + ']';
			callObject(uri);
		},
		consultationIndex:function(data){//通过menuId刷新专家咨询首页data数据格式为 {menuIds:['1','2']}
			var uri = 'mobile-service://?object=refreshPage&command=consultationIndex&params= [' + data + ']';
			callObject(uri);
		}
	}*/
};

export { boncAppEngine };
