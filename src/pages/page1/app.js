import Vue from 'vue';
import App from './app.vue';
import axios from '@/api/http';
import VueAxios from 'vue-axios';
import './cube-ui';
import 'amfe-flexible';

// import "@/utils/js/error-handle.js";

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

new Vue({
  render: h => h(App)
}).$mount('#app');
