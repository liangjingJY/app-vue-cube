let path = require('path');
let glob = require('glob');

const webpack = require('webpack');

// const autoImportCss = "./src/styles/imports.styl";

/**
 *配置pages多页面获取当前文件夹下的html和js
 *
 * @author LiangJ
 * @param {String} globPath
 * @returns
 */
function getEntry(globPath) {
  let entries = {},
    basename,
    tmp,
    pathname;

  glob.sync(globPath).forEach(function(entry) {
    basename = path.basename(entry, path.extname(entry));
    console.log(`entry=====${entry}`);
    tmp = entry.split('/').splice(-3);
    pathname = basename; // 正确输出js和html的路径

    console.log(`pathname=====${pathname}`);
    entries[pathname] = {
      entry: 'src/' + tmp[0] + '/' + tmp[1] + '/app.js',
      template: 'src/' + tmp[0] + '/' + tmp[1] + '/' + tmp[2],
      title: tmp[2],
      filename: tmp[2]
    };
  });
  return entries;
}

let pages = getEntry('./src/pages/**?/*.html');
console.log(pages);

module.exports = {
  //引用文件的相对路径
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  // 输出文件目录
  // outputDir: "dist/woApp",
  pages,
  // 生产环境是否生成 sourceMap 文件
  // productionSourceMap: false,
  devServer: {
    index: 'index.html', //启动serve 默认打开index页面
    open: process.platform === 'darwin',
    port: 8080,
    proxy: {
      '/api': {
        target: 'https://221.192.1.84:9095/SSPMobileServer',
        changeOrigin: true
        // pathRewrite: path => {
        //   return path.replace("/api", "/");
        // }
      }
    }
    // before: app => {
    //   app.post("/360ChannelView/getBasicInfo", (req, res) => {
    //     let resData = require("@/mock/data.json");
    //     res.json(resData); //接口返回json数据，上面配置的数据seller就赋值给data请求后调用
    //   });
    // }
  },
  css: {
    loaderOptions: {
      stylus: {
        'resolve url': true,
        import: ['./src/theme']
      }
    }
  },
  pluginOptions: {
    'cube-ui': {
      postCompile: true,
      theme: true
    }
  },
  chainWebpack: config => {
    //moment.js 只打包中文
    config
      .plugin('context')
      .use(webpack.ContextReplacementPlugin, [/moment[/\\]locale$/, /zh-cn/]);

    // const types = ["vue-modules", "vue", "normal-modules", "normal"];
    // types.forEach(type =>
    //   addStyleResource(config.module.rule("stylus").oneOf(type))
    // );
  }
};

/**
 *自动导入文件设置
 *
 * @author LiangJ
 * @param {*} rule
 */
// function addStyleResource(rule) {
//   rule
//     .use("style-resource")
//     .loader("style-resources-loader")
//     .options({
//       patterns: [path.resolve(__dirname, autoImportCss)]
//     });
// }
